import React from "react";
import Coords from "./Coords";
import "./App.css";

class App extends React.Component {
  state = {
    lat: null,
    long: null,
    errorMessage: "",
    off: false
  };

  showInfos = () => {
    window.navigator.geolocation.getCurrentPosition(
      position =>
        this.setState({
          lat: position.coords.latitude,
          long: position.coords.longitude,
          off: true
        }),
      err => this.setState({ errorMessage: err.message })
    );
  };

  close = () => {
    this.setState({ off: false });
  };

  render() {
    return (
      <div className="App">
        <Coords
          long={this.state.lat}
          lat={this.state.long}
          off={this.state.off}
        />
        <button onClick={this.showInfos}>Show infos!</button>
        <button onClick={this.close}>Adios!</button>
      </div>
    );
  }
}

export default App;
