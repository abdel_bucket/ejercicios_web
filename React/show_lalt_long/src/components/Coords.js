import React from "react";

class Coords extends React.Component {
  render() {
    if (!this.props.lat && !this.props.long && !this.props.off) {
      return <div>Loading..!</div>;
    } else if (!this.props.off) {
      return <div>Adios!</div>;
    }

    return (
      <div>
        <h1>Laltitude: {this.props.lat}</h1>
        <br />
        <h1>Longitud: {this.props.long}</h1>
      </div>
    );
  }
}

export default Coords;
