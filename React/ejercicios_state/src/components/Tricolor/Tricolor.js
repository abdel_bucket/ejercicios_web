import React from "react";

class Tricolor extends React.Component {
  state = { position: 0 };

  changeColor = () => {
    this.setState({ position: this.state.position + 1 });
  };

  render() {
    let colors = ["red", "green", "blue"];
    if (this.state.position === colors.length) {
      this.setState({ position: 0 });
    }
    return (
      <div
        style={{
          width: "50px",
          height: "50px",
          backgroundColor: colors[this.state.position]
        }}
        onClick={this.changeColor}
      >
        Tricolor
      </div>
    );
  }
}

export default Tricolor;
