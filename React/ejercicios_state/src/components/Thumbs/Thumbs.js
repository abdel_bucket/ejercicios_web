import React from "react";

class Thumbs extends React.Component {
  state = { thumbUp: false };

  change = () => {
    this.setState({ thumbUp: !this.state.thumbUp });
  };

  render() {
    return this.state.thumbUp ? (
      <i className="far fa-thumbs-up" onClick={this.change} />
    ) : (
      <i className="far fa-thumbs-down" onClick={this.change} />
    );
  }
}

export default Thumbs;
