import React from "react";

class FotosShow extends React.Component {
  displayImage = () => {
    switch (this.props.value) {
      case "moto":
        return (
          <img
            src="https://emcor.com.ph/wp-content/uploads/2017/11/FZI-Black.jpg"
            alt="moto"
          />
        );
      case "coche":
        return (
          <img
            src="https://media.citroen.co.uk/image/43/2/500x500_mummy_bloggers_lp.234432.jpg"
            alt="coche"
          />
        );
      case "bici":
        return (
          <img
            src="https://cdn.shopify.com/s/files/1/0077/9108/3577/products/51BRnFOjoXL_600x600.jpg?v=1526457422"
            alt="bici"
          />
        );
      case "bus":
        return (
          <img
            src="https://cdn.kanootours.com/media/catalog/product/cache/1/image/500x/9df78eab33525d08d6e5fb8d27136e95/t/o/todo-turismo-bus-uyuni.jpg"
            alt="coche"
          />
        );
      default:
    }
  };

  render() {
    return <div>{this.displayImage()}</div>;
  }
}

export default FotosShow;
