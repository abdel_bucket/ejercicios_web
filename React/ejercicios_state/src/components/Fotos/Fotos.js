import React from "react";
import FotosShow from "./FotosShow";

class Fotos extends React.Component {
  state = { selectValue: "bici" };

  chosenValue = e => {
    this.setState({ selectValue: e.target.value });
  };

  render() {
    return (
      <div>
        <select onChange={this.chosenValue} value={this.state.selectValue}>
          <option value="coche">Coche</option>
          <option value="moto">Moto</option>
          <option value="bici">Bici</option>
          <option value="bus">Bus</option>
        </select>
        <FotosShow value={this.state.selectValue} />
      </div>
    );
  }
}

export default Fotos;
