import React from "react";
import Thumbs from "./Thumbs/Thumbs";
import Tricolor from "./Tricolor/Tricolor";
import Fotos from "./Fotos/Fotos";
import Lista from "./Lista/Lista";
import ListCites from "./Combo/ListCities";

class App extends React.Component {
  render() {
    return (
      <div>
        <Thumbs />
        <Tricolor />
        <Fotos />
        <ListCites />
      </div>
    );
  }
}

export default App;
