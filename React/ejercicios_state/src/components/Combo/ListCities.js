import React from "react";
import axios from "axios";
import SelectCities from "./SelectCities";

export default class ListSelect extends React.Component {
  state = {
    prov: [],
    pob: [],
    show: false
  };

  componentDidMount() {
    axios
      .get(
        `http://kiosko.gestionproyectos.com/?controller=API&action=listadoprovin`
      )
      .then(res => {
        const response = res.data.Cp_provincias;
        this.setState({ prov: response });
      });
  }

  chosenValue = e => {
    if (this.state.prov.length !== 0) {
      this.setState({ selectValue: e.target.value });
      axios
        .get(
          `http://kiosko.gestionproyectos.com/?controller=API&action=listadoPoblaW&provin=${
            e.target.value
          }`
        )
        .then(res => {
          const response = res.data.Cp_poblacion;
          this.setState({ pob: response, show: true });
        });
    }
  };

  render() {
    const prov = this.state.prov.map((prov, i) => (
      <option key={i} value={prov.cppro_id}>
        {prov.cppro_nombre}
      </option>
    ));

    const pob = this.state.pob.map((pob, i) => (
      <option key={i} value={pob.cppob_nombre}>
        {pob.cppob_nombre}
      </option>
    ));

    return (
      <div>
        <SelectCities
          change={this.chosenValue}
          prov={prov}
          pob={pob}
          show={this.state.show}
        />
      </div>
    );
  }
}
