import React from "react";

const SelectCities = props => {
  return (
    <div>
      <select onChange={props.change} value={props.value}>
        {props.prov}
      </select>
      {props.show ? <select>{props.pob}</select> : null}
    </div>
  );
};

export default SelectCities;
