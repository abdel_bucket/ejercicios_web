import React from "react";
import Cities from "./cities.json";

class Lista extends React.Component {
  state = { input: "", ct: "" };

  onChangeHandler = e => {
    this.setState({
      input: e.target.value
    });
    console.log(this.state.ct);
  };

  getValue = city => {
    this.setState({ input: city });
  };

  render() {
    //let cities = ["Barcelona", "Madrid", "Malaga", "Valencia", "Sevilla"];
    const arrayCities = Cities.Cp_poblacion;

    const city = arrayCities
      .filter(
        city =>
          this.state.input === "" ||
          city.cppob_nombre.includes(this.state.input)
      )
      .map((city, i) => (
        <li
          style={{ marginBottom: "20px" }}
          onClick={() => this.getValue(city.cppob_nombre)}
          key={i}
        >
          {city.cppob_nombre}
        </li>
      ));

    return (
      <div>
        <ul>{city}</ul>
        <input
          type="text"
          onChange={this.onChangeHandler}
          value={this.state.input}
        />
      </div>
    );
  }
}

export default Lista;
