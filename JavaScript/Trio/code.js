let calcularButton = document.querySelector("#calcular");
let firstCicle = document.querySelector("#circle_1");
let secondCircle = document.querySelector("#circle_2");
let thirdCircle = document.querySelector("#circle_3");

calcularButton.addEventListener("click", () => {
  let firstNumber = generateNumber();
  let secondNumber = generateNumber();
  let thirdNumber = generateNumber();

  firstCicle.innerHTML = firstNumber;
  secondCircle.innerHTML = secondNumber;
  thirdCircle.innerHTML = thirdNumber;
});

function generateNumber() {
  return Math.floor(Math.random() * 49);
}
